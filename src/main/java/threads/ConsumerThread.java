package threads;


import exceptions.MaxCapacityReachException;
import beans.RequestQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsumerThread extends Thread {

    private RequestQueue requestQueue;
    private Logger logger = LoggerFactory.getLogger(ConsumerThread.class);

    public ConsumerThread(RequestQueue requestQueue) {
        this.requestQueue = requestQueue;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                long threadId = Thread.currentThread().getId();
                logger.info("Consumer {} received {}", threadId, requestQueue.getBookingRequest());
                logger.trace("Consumer {} is processing... ", threadId);
                Thread.sleep(5000);
                logger.trace("Consumer {} finished processing", threadId);
            } catch (MaxCapacityReachException e) {
                interrupt();
                logger.trace("Max capacity was reached");
            } catch (InterruptedException e) {
                logger.error("Interrupted exception");
            }

        }
    }
}
