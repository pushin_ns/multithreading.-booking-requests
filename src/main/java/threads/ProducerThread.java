package threads;

import beans.Hotel;
import exceptions.MaxCapacityReachException;
import beans.RequestQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class ProducerThread extends Thread {

    private RequestQueue requestQueue;
    private Random random = new Random();
    private Logger logger = LoggerFactory.getLogger(ProducerThread.class);

    public ProducerThread(RequestQueue requestQueue) {
        this.requestQueue = requestQueue;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Hotel hotel = new Hotel(random.nextInt(1000), "foo", "bar");
                requestQueue.addBookingRequest(hotel);
                logger.info("Producer {} sent {}", Thread.currentThread().getId(), hotel);
            } catch (MaxCapacityReachException e) {
                interrupt();
                logger.trace("Max capacity was reached");
            } catch (InterruptedException e) {
                logger.error("Interrupted exception");
            }

        }
    }
}
