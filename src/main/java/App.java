import beans.RequestQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import threads.ConsumerThread;
import threads.ProducerThread;

public class App {
    public static void main(String[] args) throws InterruptedException {

        Logger logger = LoggerFactory.getLogger(App.class);
        RequestQueue requestQueue = new RequestQueue();

        for (int i = 0; i < 3; i++) {
            (new ProducerThread(requestQueue)).start();
        }
        for (int i = 0; i < 6; i++) {
            (new ConsumerThread(requestQueue)).start();
        }

        Thread.sleep(18000);
        logger.info("Состояние очереди: {}", requestQueue.getHotelList().toString());
    }
}
