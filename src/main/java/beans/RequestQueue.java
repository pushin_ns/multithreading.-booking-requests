package beans;

import exceptions.MaxCapacityReachException;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

public class RequestQueue {
    private Queue<Hotel> hotelList;
    private static final int CAPACITY = 15;
    private volatile AtomicInteger requestCounter = new AtomicInteger(1);
    private volatile AtomicInteger bookCounter = new AtomicInteger(0);

    public RequestQueue() {
        hotelList = new LinkedList<>();
    }

    public synchronized Hotel getBookingRequest() throws InterruptedException, MaxCapacityReachException {
        if (bookCounter.incrementAndGet() <= getCAPACITY()) {
            if (this.hotelList.peek() == null)
                wait();
            return this.hotelList.poll();
        } else {
            notifyAll();
            Thread.currentThread().interrupt();
            throw new MaxCapacityReachException();
        }
    }

    public synchronized void addBookingRequest(Hotel hotel) throws InterruptedException, MaxCapacityReachException {
        if (requestCounter.incrementAndGet() <= getCAPACITY()) {
            this.hotelList.add(hotel);
            Thread.sleep(10);
            notify();
        } else {
            Thread.currentThread().interrupt();
            throw new MaxCapacityReachException();
        }
    }

    public Queue<Hotel> getHotelList() {
        return hotelList;
    }

    private static int getCAPACITY() {
        return CAPACITY;
    }

}
