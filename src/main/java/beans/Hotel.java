package beans;

public class Hotel {
    private long id;
    private String date;

    private String name;

    public Hotel(long id, String date, String name) {
        this.id = id;
        this.date = date;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", name='" + name + '\'' +
                '}';
    }


}
